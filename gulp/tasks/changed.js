const gulp = require('gulp');
const changed = require('gulp-changed');
 
const SRC = 'src/*.js';
const DEST = 'dist';

gulp.task('default', () => {
    return gulp.src(SRC)
        .pipe(changed(DEST))
        .pipe(gulp.dest(DEST));
});
