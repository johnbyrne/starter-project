var browserSync = require('browser-sync').create();

var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass');

// Static Server + watching scss/html files
gulp.task('browser-sync', ['sass'], function() {

    browserSync.init({
        server: './'
    });

    gulp.watch('./scss/*.scss', ['sass']);
    gulp.watch('./js/*.js').on('change', browserSync.reload);
    gulp.watch('./templates/**/*.html').on('change', browserSync.reload);
});


// alternative

// gulp.task('browser-sync', function() {
//     browserSync.init({
//         proxy: 'yourlocal.dev'
//     });
// });
