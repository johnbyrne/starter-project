/* File: gulpfile.js */

var gulp = require('gulp');
const requireDir = require('require-dir');
const tasks = requireDir('./gulp/tasks', {recurse: true}); // eslint-disable-line

// gulp.task('default', function() {
//   return gutil.log('Gulp is running!')
// });

// gulp.task('default', ['browser-sync', 'clean', 'sass', 'useref']);
gulp.task('default', ['browser-sync', 'sass', 'useref']);
