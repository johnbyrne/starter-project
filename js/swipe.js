(function() {

  'use strict';

  var swipe = function(options) {

    // var el = document.querySelector(options.el);
    // var swipablePages = el.querySelectorAll('.' + options.swipablePages);
    // var NUMBER_OF_PAGES = swipablePages.length;

    document.addEventListener('touchstart', handleTouchStart, false);
    document.addEventListener('touchmove', handleTouchMove, false);

    // var currentPage = 0;
    var xDown = null;
    var yDown = null;

    var animatePage = function(SWIPE_ACTION) {
      switch (SWIPE_ACTION) {
        case 'SWIPED_LEFT_TO_RIGHT':
        {
          options.clickRight();
          break;
        }
        case 'SWIPED_RIGHT_TO_LEFT':
        {
          options.clickLeft();
          break;
        }
      }
    };

    function handleTouchStart(evt) {
      xDown = evt.touches[0].clientX;
      yDown = evt.touches[0].clientY;
    };

    function handleTouchMove(evt) {
      if (! xDown || ! yDown) {
        return;
      }

      var xUp = evt.touches[0].clientX;
      var yUp = evt.touches[0].clientY;

      var xDiff = xDown - xUp;
      var yDiff = yDown - yUp;

      if (Math.abs(xDiff) > Math.abs(yDiff)) {/*most significant*/
        if (xDiff > 0) {
          /* left swipe */
          animatePage('SWIPED_RIGHT_TO_LEFT');
        } else {
          /* right swipe */
          animatePage('SWIPED_LEFT_TO_RIGHT');
        }
      } else {
        if (yDiff > 0) {
          /* up swipe */
        } else {
          /* down swipe */
        }
      }
      /* reset values */
      xDown = null;
      yDown = null;
    };
  };

  window.swipe = swipe;
})();
