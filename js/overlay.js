(function() {

  'use strict';

  var overlay = function(options) {

    var el = document.querySelector(options.el);
    var visible = true;

    var handleClick = function(link, index) {
      visible ? el.style.display = 'none' : el.style.display = 'block';
      visible = !visible;
    };

    return {
      handleClick: handleClick
    };
  };

  window.tabs = tabs;
})();
