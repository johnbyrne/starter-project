// Templates

var simpleFetch = simpleFetch();
// var templateEngine = templateEngine();

// Horizontal Tabs

var tabsTemplate = simpleFetch.fetch('./templates/tabs.tpl.html',
  function(tabsTplData) {
    simpleFetch.fetch('./json/horizontal-tabs.json', function(tabsJsonData) {
      var templateScript = Handlebars.compile(tabsTplData);
      var html = templateScript(JSON.parse(tabsJsonData));
      document.querySelector('#tabs.horizontal-tabs').innerHTML = html;
      var horizontalTabs = tabs({
        el: '#tabs.horizontal-tabs',
        tabNavigationLinks: '.tabs__nav__tab-button',
        tabContentContainers: '.tabs__active-tab__tab-container',
        activeClass: 'tabs__nav__tab-button--active',
        activeTabClass: 'tabs__active-tab__tab-container--active'
      });
      horizontalTabs.init();
    });
  });

// Vertical Tabs

var tabsTemplate = simpleFetch.fetch('./templates/tabs.tpl.html',
  function(tabsTplData) {
    simpleFetch.fetch('./json/vertical-tabs.json', function(tabsJsonData) {
      var templateScript = Handlebars.compile(tabsTplData);
      var html = templateScript(JSON.parse(tabsJsonData));
      document.querySelector('#tabs.vertical-tabs').innerHTML = html;
      var verticalTabs = tabs({
        el: '#tabs.vertical-tabs',
        tabNavigationLinks: '.tabs__nav__tab-button',
        tabContentContainers: '.tabs__active-tab__tab-container',
        activeClass: 'tabs__nav__tab-button--active',
        activeTabClass: 'tabs__active-tab__tab-container--active'
      });
      verticalTabs.init();
    });
  });

// Carousel

var carousel = carousel({
  el: '#carousel',
  carouselSections: '.carousel__section'
});

carousel.init();
