(function() {

  'use strict';

  var carousel = function(options) {
    var el = document.querySelector(options.el);
    var carouselSections = el.querySelectorAll(options.carouselSections);
    var currentSectionIndex = 0;
    var previousSectionIndex = 0;
    var initCalled = false;

    var init = function() {
      if (!initCalled) {
        initCalled = true;
        setInterval(function() {
          if  (++currentSectionIndex > carouselSections.length - 1) {
            currentSectionIndex = 0;
          }
          carouselSections[currentSectionIndex].classList.remove('carousel__section');
          carouselSections[currentSectionIndex].classList.add('carousel__section--active');
          carouselSections[previousSectionIndex].classList.remove('carousel__section--active');
          carouselSections[previousSectionIndex].classList.add('carousel__section');
          previousSectionIndex = currentSectionIndex;
        }, 500);
      }
    };

    return {
      init: init
    };
  };

  window.carousel = carousel;
})();
