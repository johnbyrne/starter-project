(function() {

  simpleFetch = function() {

    fetch = function(path, callback) {
      var request = new XMLHttpRequest();
      var params = "action=something";
      request.open('GET', path, true);
      request.onreadystatechange = function() {
        if (request.readyState == 4) {
          // alert("It worked!");
        }
      };
      request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
      // request.setRequestHeader("Content-length", params.length);
      // request.setRequestHeader("Connection", "close");
      request.send(params);
      request.onload = function(data) {
        callback(this.response);
      }.bind(request);
    };

    return {
      fetch: fetch
    };
  }

  window.simpleFetch = simpleFetch;
})();
