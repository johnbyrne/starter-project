// Swipable pages

var simpleFetch = simpleFetch();

var currentPageCanvas = 0;
var mainPage = document.querySelector('.main-page');
var otherPage = document.querySelector('.other-page');

var swipablePagesTemplate = simpleFetch.fetch('./templates/swipable-pages.tpl.html',
  function(swipablePagesTplData) {
    simpleFetch.fetch('./json/swipable-pages.json',
      function(swipablePagesJsonData) {
        var templateScript = Handlebars.compile(swipablePagesTplData);
        var html = templateScript(JSON.parse(swipablePagesJsonData));
        document.querySelector('.js_rewind').innerHTML = html;

        var rewind = document.querySelector('.js_rewind');
        var rewind2 = document.querySelector('.js_rewind2');

        lory(rewind, {
           rewind: true,
           enableMouseEvents: true
         });
        // lory(rewind2, {
        //    rewind: true,
        //    enableMouseEvents: true
        //  });

        document.getElementsByClassName('prev')[0].addEventListener('click', function(e) {
          // debugger;
          // var index = lory(returnIndex);
          var activeSlideIndex = 0;
          var slides = document.querySelectorAll('.js_slide');
          for (var i = 0; i < slides.length; i++) {
            // debugger;
            var slideIsActive = ((String(slides[i].classList).indexOf('active')) > -1);
            console.log('Slide is active? ' + slideIsActive);
            if (slideIsActive) {
              activeSlideIndex = i;
            }
          }
          if (activeSlideIndex === 0) {
            // user clicked left, and no more slides
            // animate new page
            currentPageCanvas = 1 - currentPageCanvas;

            if (currentPageCanvas === 1) {
              // position other canvas at 100% and animate to 0%
              otherPage.style.left = '0%';
              // transition main canvas from 0% to -100%
              mainPage.style.right = '100%';

              setTimeout(function() {
                // This coincides with the transition completing
              }, 1000);
            }
          }
        });
      });
    });
